// Require dependencies
const mongoose = require('mongoose')

// creates and intitializes the activity schema
const ActivitySchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true,
        unique: true
    },
    transactionId: {
        type: String,
        required: true,
        unique: true
    },
    dateCreated: {
        type: Date,
        default: Date.now
    },
    dateUpdated: {
        type: Date,
        default: Date.now
    },
    requests: [
        {
            title: String,
            content: String,
            userId: String,
            amountRequested: Number,
            settlementDate: String,
            audience: {
                type: String,
                required: true
            },
            accepted: {
                type: Boolean,
                default: false
            },
            resolved: {
                type: Boolean,
                default: false
            },
            dateCreated: {
                type: Date,
                default: Date.now
            },
            dateUpdated: {
                type: Date,
                default: Date.now
            }
        }
    ],
    transactionActivity: [
        {
            title: String,
            content: String,
            dateCreated: {
                type: Date,
                default: Date.now
            }
        }
    ],
    userActivity: [
        {
            title: String,
            content: String,
            dateCreated: {
                type: Date,
                default: Date.now
            }
        }
    ],
    customerServiceMessages: [
        {
            title: String,
            message: String,
            dateCreated: {
                type: Date,
                default: Date.now
            }
        }
    ]
})

// Exports the user model schema
module.exports = mongoose.model('Activity', ActivitySchema)