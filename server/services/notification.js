const axios = require('../utils/axios')
const { isEmpty } = require('lodash')
const { pocketloanNotification } = require('../../config')

/**
 * @class {Object} Notification
 * @constructor {Object} Options
 * sends users notifications through the notification service
 */
class Notification {
    constructor(options = {}) {
        this.options = options
    }

    /**
     * @param {String} template
     * @param {String} recipientAddress
     * @param {String} first_name
     * @param {String} email_message
     * @returns {Promise}
     * sends users email notifications through the notification service
     */
    async sendEmail(template, recipientAddress, first_name, email_message) {
        return axios.post(`${pocketloanNotification}/v1/api/notify/send-email`, {
            template, recipientAddress, first_name, email_message
        }).then((response) => {
            return response
        }).catch((err) => {
            if (err) { throw err }
        })
    }

    /**
     * @param {String} body
     * @param {String} to
     * @returns {Promise}
     * sends users textmessage notifications through the notification service
     */
    async sendTextMessage(body, to) {
        return axios.post(`${pocketloanNotification}/v1/api/notify/send-text`, { body, to })
            .then((response) => {
                return response
            }).catch((err) => {
                if (err) { throw err }
            })
    }

    /**
     * @param {String} message
     * @returns {Promise}
     * sends message to selected slack users
     */
    async slackBot(message, options) {
        let params = ''
        if (!isEmpty(options) && options.isCustomerServiceMessage === true) {
            params = '?isCustomerServiceMessage=true'
        }
        return axios.post(`${pocketloanNotification}/v1/api/notify/slackbot${params}`, { message }).then((response) => {
            return response
        }).catch((err) => {
            if (err) { throw err }
        })
    }

    /**
     * @param {String} length
     * @param {String} tokenType
     * @returns {Promise}
     * generates token from the pocketloan notification service
     */
    async generateToken(length, tokenType) {
        return axios.post(`${pocketloanNotification}/v1/api/notify/token`, { length, tokenType })
            .then((response) => {
                return response
            }).catch((err) => {
                if (err) { throw err }
            })
    }

}

// exports the Notification class
module.exports = Notification