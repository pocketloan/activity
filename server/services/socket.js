
const jwtDecode = require('jwt-decode')
const { isEmpty, merge } = require('lodash')
const SlackBot = require('slackbots')
const { slackConfig } = require('../../config')
const NotificationService = require('../services/notification')

const slack = new SlackBot(slackConfig)
const notify = new NotificationService()

const handleChat = (chat) => {
    chat.on('connect', async (socket) => {
        let token = socket.handshake.query.token
        let decodedToken = await decode(token)
        socket.on('newMessage', async (data) => {
            const { message } = data
            await notify.slackBot(message, { isCustomerServiceMessage: true })
        })
        slack.on('message', (data) => {
            if (data.channel === 'CCKKX5WNS' && data.type === 'message' && isEmpty(data.subtype)) {
                const { userId } = decodedToken
                const payload = merge({ userId }, data)
                socket.emit('customer_service', payload)
            }
        })
        socket.emit('connected', { message: 'welcome to pocketloan chat', badgeCount: '1' })
    })
}

const handleNotify = (notify) => {
    notify.on('connect', (socket) => {
        let token = socket.handshake.query.token
        // if (validate(token)) { }
        socket.emit('connected', { notifyCount: 1 })
    })
}

const decode = async (token) => {
    if (!isEmpty(token) && typeof token !== 'undefined') {
        decoded = await jwtDecode(token)
        return decoded
    }
}

const validate = (decoded) => {

}

module.exports = (io) => {
    handleChat(io.of('/chat'))
    handleNotify(io.of('/notify'))
}