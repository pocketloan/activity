// Require dependencies
require('../models/activity')
const { isArray, isEmpty } = require('lodash')
const Mongoose = require('mongoose')
const Mongo = require('./mongo')


/** 
 * Connects and fetches data from the database
 * All database requests should be gotten with the ActivityDao
 * @class {Object} Activity Data Access Object
 * @param {Object} options
 */
class ActivityDoa {
    constructor(options = {}) {
        this.options = options
        this.mongo = new Mongo()
        this.mongoose = Mongoose
        this.activity = this.mongoose.model('Activity')
        this.connectToDatabase()
    }

    /**
     * Retrieves all activities
     * @returns {[Object]} List of activity objects
     */
    async getActivities() {
        return this.activity.find({}, (err, activities) => {
            this._handleError(err)
            return activities
        })
    }

    /**
     * Retrieves activity by id
     * @returns {Object} Activity Object
     */
    async getActivity(id) {
        return this.activity.findById(id, (err, activity) => {
            this._handleError(err)
            return activity
        })
    }

    /**
     * Retrieves activity by user id
     * @returns {Object} Activity Object
     */
    async getActivityByUserId(id) {
        return this.activity.findOne({ userId: id }, (err, activity) => {
            this._handleError(err)
            return activity
        })
    }

    /**
     * Retrieves activity by user id
     * @returns {Object} Activity Object
     */
    async getActivityByTransactionId(id) {
        return this.activity.findOne({ transactionId: id }, (err, activity) => {
            this._handleError(err)
            return activity
        })
    }

    /**
     * Creates a new activity
     * @param {[String]} userId - User id
     * @param {[String]} transactionId - Transaction id
     * @returns {Object} A new activity object
     */
    async createActivity({ transactionId, userId }) {
        const newActivity = new this.activity({ transactionId, userId })
        newActivity.dateCreated = new Date().toISOString()
        newActivity.dateUpdated = newActivity.dateCreated
        return newActivity.save().then(activity => activity).catch(err => { throw err })
    }

    /**
     * Updates an activity by id
     * @param {String} id - activity id
     * @param {Object} fields - fields to be updated
     * @returns {Object} Updated activity Object
     */
    async updateActivity(_id, fields) {
        if (isArray(fields)) {
            return this.activity.findOne({ _id }, (err, activity) => {
                this._handleError(err)
                activity['dateUpdated'] = new Date().toISOString()
                activity[fields[0]] = fields[1]
                activity.save((err, updated) => {
                    this._handleError(err)
                    return updated
                })
            })
        }
        return this.activity.updateOne({ _id }, { $addToSet: fields }, err => this._handleError(err))
    }

    /**
     * Updates an activity by user id
     * @param {String} id - user id
     * @param {Object} fields - fields to be updated
     * @returns {Object} Updated activity Object
     */
    async updateActivityByUserId(userId, fields) {
        if (isArray(fields)) {
            return this.activity.findOne({ userId }, (err, activity) => {
                this._handleError(err)
                activity['dateUpdated'] = new Date().toISOString()
                activity[fields[0]] = fields[1]
                activity.save((err, updated) => {
                    this._handleError(err)
                    return updated
                })
            })
        }
        let isDuplicate = await this._checkDuplicate({ userId, fields })
        return (!isDuplicate) ? this.activity.updateOne({ userId }, { $addToSet: fields }, err => this._handleError(err)) : {}
    }

    /**
     * Updates an activity by transaction id
     * @param {String} id - transaction id
     * @param {Object} fields - fields to be updated
     * @returns {Object} Updated activity Object
     */
    async updateActivityByTransactionId(transactionId, fields) {
        if (isArray(fields)) {
            return this.activity.findOne({ transactionId }, (err, activity) => {
                this._handleError(err)
                activity['dateUpdated'] = new Date().toISOString()
                activity[fields[0]] = fields[1]
                activity.save((err, updated) => {
                    this._handleError(err)
                    return updated
                })
            })
        }
        return this.activity.updateOne({ transactionId }, { $addToSet: fields }, err => this._handleError(err))
    }

    /**
     * Deletes an activity. Important!! you dont want to do this unless user deletes his/her pocketloan account
     * @param {String} _id - activity id
     */
    async deleteActivity(_id) {
        return this.activity.deleteOne({ _id }, err => this._handleError(err))
    }

    /**
     * Deletes an activity. Important!! you dont want to do this unless user deletes his/her pocketloan account
     * @param {String} userId - user id
     */
    async deleteActivityByUserId(userId) {
        return this.activity.deleteOne({ userId }, err => this._handleError(err))
    }

    /**
     * Deletes an activity. Important!! you dont want to do this unless user deletes his/her pocketloan account
     * @param {String} transactionId - transaction id
     */
    async deleteActivityByTransactionId(transactionId) {
        return this.activity.deleteOne({ transactionId }, err => this._handleError(err))
    }

    /**
     * @return {Object} MongooseConnection
     * connects to the database
     */
    async connectToDatabase() {
        return await this.mongo.connect()
    }

    async _checkDuplicate({ userId, fields }) {
        let result = false
        const activity = await this.getActivityByUserId(userId)
        if (!isEmpty(fields.userActivity) && activity.userActivity.length > 0) {
            const lastUserActivity = activity.userActivity[activity.userActivity.length - 1]
            result = (fields.userActivity.content === lastUserActivity.content) ? true : false
        }
        return result
    }

    /**
     * Handles all errors in the Activity
     * @param {Error} err - error object instance
     * @throws {Error}
     */
    _handleError(err) {
        if (err) {
            throw err
        }
    }
}

// exports the ActivityDao
module.exports = ActivityDoa