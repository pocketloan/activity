const HomeRouter = require('./home')
const HealthRouter = require('./health')
const ActivityRouter = require('./activity')
const HealthService = require('../services/health')
const NotificationService = require('../services/notification')
const Middleware = require('../services/middleware')
const ActivityDao = require('../dao/activity')

// Exports and Initializes the application routes
module.exports = ({ app, io }) => {
    const IO = io
    const handlers = [
        new HomeRouter({ Middleware }),
        new HealthRouter({ HealthService, NotificationService }),
        new ActivityRouter({ NotificationService, ActivityDao, IO })
    ]

    handlers.forEach(handler => {
        handler.init(app)
    })
}