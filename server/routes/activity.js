const Router = require('koa-router')
const log = require('custom-logger')
const { SERVER_ERROR } = require('../errorMessages')
const { version } = require('../../config')

class ActivityRouter extends Router {
    constructor(options = {}) {
        super()
        this.options = options
        this.notificationService = new options.NotificationService()
        this.activityDao = new options.ActivityDao()
        options.IO.of('/notify').on('connect', (socket) => {
            this.socket = socket
        })
    }

    // Initializes the application route
    init(app) {
        this.prefix(`${version}`)
        // GET REQUESTS
        this.get('/', ctx => this.getActivities(ctx))
        this.get('/:id', ctx => this.getActivity(ctx))
        this.get('/users/:id', ctx => this.getActivityByUserId(ctx))
        this.get('/transactions/:id', ctx => this.getActivityByTransactionId(ctx))
        // POST REQUESTS
        this.post('/', ctx => this.createActivity(ctx))
        this.post('/dispatch', ctx => this.dispatchRequest(ctx))
        // PATCH REQUESTS
        this.patch('/:id', ctx => this.updateActivity(ctx))
        this.patch('/users/:id', ctx => this.updateActivityByUserId(ctx))
        this.patch('/transactions/:id', ctx => this.updateActivityByTransactionId(ctx))
        // DELETE REQUESTS
        this.delete('/:id', ctx => this.deleteActivity(ctx))
        this.delete('/users/:id', ctx => this.deleteActivityByUserId(ctx))
        this.delete('/transactions/:id', ctx => this.deleteActivityByTransactionId(ctx))

        app.use(this.routes())
    }

    /**
     * Gets all activities
     * @param {Object} ctx
     * @returns {[Object]} koa context body
     */
    async getActivities(ctx) {
        try {
            const activities = await this.activityDao.getActivities()
            ctx.body = {
                message: 'successfully retrieved all activities',
                status: 200,
                activities
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in retrieving activities`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Gets a particular activity by id
     * @param {Object} ctx
     * @returns {Object} koa context body
     */
    async getActivity(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.getActivity(id)
            ctx.body = {
                message: 'successfully retrieved activity by id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in retrieving activity by id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Gets a particular activity by user id
     * @param {String} ctx[params][id] - user id
     * @returns {Object} koa context body
     */
    async getActivityByUserId(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.getActivityByUserId(id)
            ctx.body = {
                message: 'successfully retrieved activity by user id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in retrieving activity by id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Gets a particular activity by transaction id
     * @param {String} ctx[params][id] - transaction id
     * @returns {Object} koa context body
     */
    async getActivityByTransactionId(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.getActivityByTransactionId(id)
            ctx.body = {
                message: 'successfully retrieved activity by transaction id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in retrieving activity by id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * creates a new activity
     * @param {String} ctx[request][body][userId] - user Id
     * @param {String} ctx[request][body][transaction] - transaction Id
     * @returns {Object} koa context body
     */
    async createActivity(ctx) {
        try {
            const activity = await this.activityDao.createActivity(ctx.request.body)
            ctx.body = {
                message: 'pocketloan activity tracker successfully created for user',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in creating activity`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    async dispatchRequest(ctx) {
        try {
            const isOwing = await this._isOwing(ctx.request.body)
            if (!isOwing) {
                const activities = await this.activityDao.getActivities()
                activities.forEach(async (activity) => {
                    await this.activityDao.updateActivityByUserId(activity.userId, { requests: ctx.request.body })
                })
                const { title, userId, content, amountRequested } = ctx.request.body
                await this.activityDao.updateActivityByUserId(userId, {
                    transactionActivity: {
                        title,
                        content: content.replace('A borrower just', 'You'),
                        amountRequested
                    }
                })
                await this.socket.emit('increment', { request: ctx.request.body })
                await this.notificationService.slackBot(`A Borrower just made a request, title: ${title}, content: ${content}`)
                ctx.body = {
                    message: 'Request successfullly dispatched to all loaners',
                    status: 200
                }
            } else {
                ctx.body = {
                    message: `Request wasn't dispatched due to outstanding fees owed`,
                    status: 409
                }
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in dispatching a borrowers request`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * updates activity by id
     * @param {String} ctx[params][id] - activity id
     * @param {String} ctx[request][body][fields] - fields to be updated
     * @returns {Object} koa context body
     */
    async updateActivity(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.updateActivity(id, ctx.request.body)
            ctx.body = {
                message: 'successfully updated activity by id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in updating activity by id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * updates activity by user id
     * @param {String} ctx[params][id] - user id
     * @param {String} ctx[request][body][fields] - fields to be updated
     * @returns {Object} koa context body
     */
    async updateActivityByUserId(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.updateActivityByUserId(id, ctx.request.body)
            ctx.body = {
                message: 'successfully updated activity by user id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in updating activity by user id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
    * updates activity by transaction id
    * @param {String} ctx[params][id] - transaction id
    * @param {String} ctx[request][body][fields] - fields to be updated
    * @returns {Object} koa context body
    */
    async updateActivityByTransactionId(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.updateActivityByTransactionId(id, ctx.request.body)
            ctx.body = {
                message: 'successfully updated activity by transaction id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in updating activity by transaction id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * deletes activity by id
     * @param {String} ctx[params][id] - activity id
     * @returns {[Object]} koa context body
     */
    async deleteActivity(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.deleteActivity(id)
            ctx.body = {
                message: 'successfully deleted activity by id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in deleting activity by id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * deletes activity by user id
     * @param {String} ctx[params][id] - user id
     * @returns {[Object]} koa context body
     */
    async deleteActivityByUserId(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.deleteActivityByUserId(id)
            ctx.body = {
                message: 'successfully deleted activity by user id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in deleting activity by user id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * deletes activity by transaction id
     * @param {String} ctx[params][id] - transaction id
     * @returns {[Object]} koa context body
     */
    async deleteActivityByTransactionId(ctx) {
        try {
            const { id } = ctx.params
            const activity = await this.activityDao.deleteActivityByTransactionId(id)
            ctx.body = {
                message: 'successfully deleted activity by transaction id',
                status: 200,
                activity
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-activity err: ${err.stack.toString()}`)
            log.debug(`err in deleting activity by transaction id`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    async _isOwing({ userId }) {
        let response = false
        const activity = await this.activityDao.getActivityByUserId(userId)
        if (activity.requests.length > 0) {
            activity.requests.forEach(request => {
                if (userId === request.userId && !request.resolved) {
                    response = true
                }
            })
        }
        return response
    }
}

// exports the ActivityRouter class
module.exports = ActivityRouter