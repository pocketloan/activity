const errorMessages = {
    UNHEALTHY_APP: {
        message: 'app is unhealthy. it cannot connect to th database',
        status: 500,
        title: 'Unhealthy Application'
    },
    SERVER_ERROR: {
        status: 500,
        message: 'Internal Server Error'
    }
}

module.exports = errorMessages