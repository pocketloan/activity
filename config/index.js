const hasAccess = require('../server/utils/allowedServices')
const PORT = process.env.PORT || 3001

module.exports = {
    PORT,
    uri: process.env.MONGO_URI,
    version: process.env.VERSION,
    jwtSecret: process.env.JWT_SECRET,
    pocketloanNotification: process.env.POCKETLOAN_NOTIFY,
    pocketloanApi: process.env.POCKETLOAN_API,
    pocketloanTransactions: process.env.POCKETLOAN_TRANSACTIONS,
    env: (req) => (req.header.host.includes(`localhost:${PORT}`)) ? 'development' : 'production',
    hasAccess,
    slackConfig: {
        token: process.env.SLACK_TOKEN,
        name: process.env.SLACK_BOT
    }
}